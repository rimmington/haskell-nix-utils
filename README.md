# haskell-nix-utils

Various [haskell.nix][]-related utilities.

[haskell.nix]: https://input-output-hk.github.io/haskell.nix
