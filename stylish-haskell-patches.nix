{ fetchpatch }:

[
  # Better support for datakinds in records
  (fetchpatch {
    url = "https://github.com/jaspervdj/stylish-haskell/pull/332.patch";
    sha256 = "1vhf4m2x0n66gzgblm0wf2l1v0c1c8m264f9z8hqwnc1myh5s97s";
  })
  # Add support for post qualified import formatting
  (fetchpatch {
    url = "https://github.com/jaspervdj/stylish-haskell/pull/335.patch";
    sha256 = "0ig79m2qrvqyngg4rryifsh98hzz3s5w0cdzs8pzvifp9lfm4ihz";
  })
  # Add break_newtypes flag
  (fetchpatch {
    url = "https://github.com/jaspervdj/stylish-haskell/pull/343.patch";
    sha256 = "082jqvknbbfjpxkzbj3r5xp060gbwq3dc81mjp5ry98g0339al47";
  })
  # Don't remove existential quantification
  (fetchpatch {
    url = "https://github.com/haskell/stylish-haskell/commit/3a15320ded7c441b90671e9b0793a955329b4891.patch";
    sha256 = "1a1j3m9r55rkfan99qrl2r2x8ky6nsnfpqbmwx47jzbfjqqli4rn";
  })
]
