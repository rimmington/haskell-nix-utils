{ lib, ... }:
with lib;
with lib.types;
{
  _file = "haskell-nix-utils/stackage-module.nix";
  options = {
    resolver = mkOption {
      type = str;
    };
    cabalFile = mkOption {
      type = nullOr path;
      default = null;
    };
    fromHackage = mkOption {
      type = attrsOf str;
      default = {};
    };
    fromSrc = mkOption {
      type = attrsOf unspecified;
      default = {};
    };
    pkg-def-extras = mkOption {
      type = listOf unspecified;
      default = [];
    };
    modules = mkOption {
      type = listOf unspecified;
      default = [];
    };
    checkMaterialization = mkOption {
      type = nullOr bool;
      default = null;
      description = "If true the nix files will be generated used to check plan-sha256 and material";
    };
  };
}
