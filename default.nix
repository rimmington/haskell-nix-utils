{ pkgs
, haskell-nix ? pkgs.haskell-nix
, gitignoreSrc ? pkgs.fetchFromGitHub {
    owner = "hercules-ci";
    repo = "gitignore.nix";
    rev = "a20de23b925fd8264fd7fad6454652e142fd7f73";
    sha256 = "sha256-8DFJjXG8zqoONA1vXtgeKXy68KdJL5UaXR8NtVMUbx8=";
  }
}@topArgs:

let
  gitignore = import gitignoreSrc { inherit (pkgs) lib; };
  dockerPkgs = import (builtins.fetchTarball {
    # https://github.com/NixOS/nixpkgs/pull/122608
    url = "https://github.com/adrian-gierakowski/nixpkgs/archive/c2f1a7d9c8e6efb033bd0cbd0c646a0ba2afde55.tar.gz";
    sha256 = "0qirywq13j29kfzzgdmqwydj8vc5lgr6zvsfl1ffr0j7h3a6by91";
  }) { inherit (pkgs) system; overlays = [ (import "${dockerNixpkgs}/overlay.nix") ]; };
  dockerNixpkgs = builtins.fetchTarball {
    url = "https://github.com/nix-community/docker-nixpkgs/archive/e8507994287495423b398721cddc4c446bbbb6bb.tar.gz";
    sha256 = "14jmmfs1m8mb1db3axyqf97w92xz0gx8j4qgqbmhcqpqm0g5w4fn";
  };
  recall = args: import ./. (topArgs // args);
in rec {
  inherit (gitignore) gitignoreFilter;

  hackage-sets = import ./hackage-sets.nix;
  stylish-haskell-patches = pkgs.callPackage ./stylish-haskell-patches.nix {};

  callCabalToNix =
    { src
    , compilerNixName ? null  # in the style "ghc8104"
    , cabalFile ? null
    }:
      let
        actualCabal =
          if cabalFile == null
            then "${src}/*.cabal"
            else cabalFile;
        drvName =
          if cabalFile == null
            then builtins.baseNameOf src
            else builtins.baseNameOf cabalFile;
        nixFile = pkgs.runCommand "cabal-to-nix-${drvName}"
          {
            nativeBuildInputs = [
              # https://github.com/input-output-hk/haskell.nix/pull/2010
              (if builtins.hasAttr "nix-tools-set" haskell-nix
                then haskell-nix.nix-tools
                else haskell-nix.nix-tools.${compilerNixName})
            ];
          }
          ''
            export LANG=C.utf8
            cabal-to-nix ${actualCabal} > $out
          '';
      in args: import nixFile args // { src = pkgs.lib.mkDefault src; };

  cleanGitHaskellSource =
    { src
    , name ? "${builtins.baseNameOf src}-src"
    }:
      let
        filter = gitHaskellSourceFilter src;
      in builtins.toPath (pkgs.lib.cleanSourceWith {
        inherit filter name src;
      });

  gitHaskellSourceFilter = src:
    let
      gitF = gitignoreFilter src;
      hsF = haskell-nix.haskellSourceFilter;
    in name: type:
      !(type == "directory" && baseNameOf name == "nix") &&
      gitF name type &&
      hsF name type;

  mkStackagePkgSet' =
    { resolver
    , fromSrc ? {}
    , fromHackage ? {}
    , extras ? _ : {}
    , modules ? []
    , pkg-def-extras ? []
    }:
      let
        compilerNixName = pkgSet.config.compiler.nix-name;
        loadSrc = args: callCabalToNix (args // { inherit compilerNixName; });
        stack-pkgs = {
          inherit resolver;
          extras = hackage:
               (builtins.mapAttrs (p: v: hackage.${p}.${v}.revisions.r0) fromHackage)
            // (builtins.mapAttrs (_: loadSrc) fromSrc)
            // extras hackage;
        };
        pkgSet = haskell-nix.mkStackPkgSet {
          inherit stack-pkgs pkg-def-extras;
          modules =
            # https://github.com/input-output-hk/haskell.nix/issues/1278
            # All components in the snapshot are planned
            [ { planned = true; } ]
            ++ modules;
        };
      in pkgSet;

  mkStackagePkgSet = args:
    let p = mkStackagePkgSet' args;
    in p.config.hsPkgs // { _config = p.config; };

  stackageProject' = projectModule:
    haskell-nix.haskellLib.evalProjectModule ./stackage-module.nix projectModule (
    args:
    let
      # https://github.com/input-output-hk/haskell.nix/pull/2052
      config = args.config or args;
      inherit (config) name src cabalFile modules;
      pkg-set = mkStackagePkgSet' {
        inherit (config) resolver fromHackage pkg-def-extras;
        fromSrc = { ${name} = { inherit src cabalFile; }; } // config.fromSrc;
        modules = modules ++ [
          {
            packages.${name}.package = {
              isLocal = true;
              isProject = true;
            };
          }
        ];
      };
      buildProject = if pkgs.stdenv.hostPlatform != pkgs.stdenv.buildPlatform
        then (recall { pkgs = pkgs.buildPackages; }).stackageProject' projectModule
        else project;
      project = haskell-nix.addProjectAndPackageAttrs {
        inherit (pkg-set.config) hsPkgs;
        inherit pkg-set;
        args = config;
        # tool = final.buildPackages.haskell-nix.tool' evalPackages pkg-set.config.compiler.nix-name;
        # tools = final.buildPackages.haskell-nix.tools' evalPackages pkg-set.config.compiler.nix-name;
        roots = pins { compilerNixName = pkg-set.config.compiler.nix-name; };
        projectFunction = haskell-nix: (recall { inherit haskell-nix; }).stackageProject';
        inherit projectModule buildProject;
      };
    in project);

  stackageProject = args:
    let p = stackageProject' args;
    in p.hsPkgs // p;

  hackageLinkedDoc = doc: pkgs.runCommand "${doc.name}-hackage-linked" {} ''
    cp -r --no-preserve=mode ${doc}/* $out
    find $out -type f -exec sed -i -E \
      -e 's#file:///nix/store/[0-9a-z-]+-lib-([0-9a-z-]+)-([0-9.]+)-haddock-doc/share/doc/[^/]+/html#https://hackage.haskell.org/package/\1-\2/docs#g' \
      -e 's#file:///nix/store/[0-9a-z-]+-ghc-[0-9.]+-doc/share/doc/ghc/html/libraries/([0-9a-z-]+)-([0-9.]+)/#https://hackage.haskell.org/package/\1-\2/docs/#g' \
      {} \;
  '';

  pins =
    { compilerNixName ? null # in the style "ghc8104"
    , nixpkgs ? haskell-nix.sources.nixpkgs
    , extraPaths ? []
    }: pkgs.linkFarm "source-pins" (
      [
        { name = "gitignore"; path = gitignoreSrc; }
        { name = "nixpkgs"; path = nixpkgs; }
        { name = "haskell-nix"; path = haskell-nix.source-pins; }
        { name = "old-ghc-nix"; path = haskell-nix.sources.old-ghc-nix; }
      ] ++ pkgs.lib.optionals (compilerNixName != null)
      [
        { name = "stdenv"; path = pkgs.stdenv; }
        { name = "stdenvNoCC"; path = pkgs.stdenvNoCC; }
        { name = "spdx"; path = pkgs.spdx-license-list-data; }
        { name = "haskell-nix-roots"; path = haskell-nix.roots compilerNixName; }
      ] ++ extraPaths
    );

  baseImage =
    let
      reverse = [ "reverse" ];
      limit = l: [ "limit_layers" l ];
      popcon = [ "popularity_contest" ];
      cut = pkg: main: rest: [
        [ "subcomponent_out" [ pkg ] ]
        [ "over" "main" main ]
        [ "over" "rest" rest ]
      ];
      pipe = pipe: [ "pipe" pipe ];
    in { name
       , compilerNixName
       , extraPaths ? []
       , extraPackages ? []
       }:
      pkgs.callPackage "${dockerNixpkgs}/images/nix" {
        dockerTools = dockerPkgs.dockerTools // {
          # Trick it into bulding a layered image
          buildImageWithNixDb = o: dockerPkgs.dockerTools.buildLayeredImageWithNixDb (o // {
            inherit name;
            debug = true;
            layeringPipeline =
              cut pkgs.gitMinimal (pipe [popcon (limit 40)]) (
                pipe [popcon reverse (limit 70)]
              );
          });
        };
        extraContents = extraPackages ++ [
          pkgs.gnused pkgs.ponysay

          (pins {
            inherit compilerNixName extraPaths;
          })
        ];
      };
}
