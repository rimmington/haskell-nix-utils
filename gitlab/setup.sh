set -euo pipefail

mkdir -p /etc/nix
cat > /etc/nix/nix.conf << EOF
# default from nixos image
sandbox = false

# max-jobs is about the number of derivations that Nix will build in parallel
max-jobs = auto

# cores is about parallelism inside a derivation, e.g. what make -j will use
# 0 means use all available cores
cores = 0

experimental-features = nix-command flakes

# binary caches
substituters = https://cache.nixos.org/ https://cache.iog.io $NIX_SUBSTITUTERS
trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ= $NIX_PUBLIC_KEYS
EOF

if [ -n "$1" ]; then
  export NIX_PATH=nixpkgs=$(realpath $1)
fi
