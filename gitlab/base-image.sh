#!/usr/bin/env nix-shell
#!nix-shell -i bash -p ponysay -p gnused -p coreutils -p skopeo

set -euo pipefail

imageName="$1"
shift

res=0

nix-build "$@" && \
    skopeo copy docker-archive:$(readlink result) docker://$CI_REGISTRY_IMAGE/$imageName:latest \
      --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD --insecure-policy \
    || res=$?

if [ $res -eq 0 ]; then
  2>/dev/null ponysay --pony pinkie -b ascii -- "It worked!"
else
  2>/dev/null ponysay --pony pinkamena -b ascii -- "It broke..."
fi

exit $res
