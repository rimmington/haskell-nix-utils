{
  outputs = {...}: {
    overlays.default = (final: prev: {
      haskell-nix-utils = import ./. { pkgs = final; };
    });
  };
}
