#!/usr/bin/env bash
set -euo pipefail

STRIP_PACKAGES="lsp|lsp-types|lsp-test|Diff|hie-bios"
ALLOW_NEWER=optparse-applicative
JUST_EXCLUDE="rts|alex|ghci"

if [[ $# -ne 2 ]]; then
    echo >&2 Usage: hls-pins.sh stackage-version hls-version
    echo >&2 eg. hls-pins.sh lts-22.43 2.9.0.1
    exit 2
fi
if ! which ghc >/dev/null 2>/dev/null; then
    echo >&2 nr ghc cabal-install
    exit 2
fi
stackage_version=$1
hls_version=$2
cabal_config="${stackage_version}.cabal.config"

mkdir -p tmp/
cd tmp

if [ ! -f "${cabal_config}" ]; then
    curl "https://www.stackage.org/${stackage_version}/cabal.config" > "${cabal_config}"
fi
cat >fake.cabal <<EOF
cabal-version:      3.0
name:               hnu-fake
version:            0.1.0.0
author:             nobody
maintainer:         nobody@example.com
build-type:         Simple

executable hnu-fake
    main-is:          Main.hs
    build-depends:    haskell-language-server==${hls_version}
EOF
if [ ! -f ~/.cache/cabal/packages/hackage.haskell.org/01-index.tar ]; then
    cabal update
fi

grep -Ev "^(constraints :|\s+)($STRIP_PACKAGES) ==" "${cabal_config}" >cabal.project.local
echo active-repositories: hackage.haskell.org:merge >>cabal.project.local
echo 'packages: *.cabal' >>cabal.project.local

cabal freeze --project-file=cabal.project.local --allow-newer="${ALLOW_NEWER}"
sed -nE 's/^\s*any\.//p' cabal.project.local.freeze | sort >cabal-picked
sed -nE 's/^(constraints :|\s+)//p' "${cabal_config}" | sort >stackage-has
installed="$(sed -nE 's/^\s+(\S+) installed,$/\1/p' "${cabal_config}" | tr '\n' '|')"
grep -Ev "^(${installed}${JUST_EXCLUDE}) " cabal-picked >cabal-picked-selectable
comm -23 cabal-picked-selectable stackage-has | sed 's/ ==/ = "/;s/,/";/'
